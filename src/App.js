import React from 'react';
import './App.css';
import { FilterContextProvider } from './filter.context';
import { Main } from './components/Main';

function App() {
  return (
    <div className="App">
      <FilterContextProvider>
        <Main></Main>
      </FilterContextProvider>
    </div>
  );
}

export default App;
