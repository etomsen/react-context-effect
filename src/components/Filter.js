import React from 'react';

export function Filter({filter, onChange}) {
  
  function onInputChange(event) {
    onChange({...filter, checked: !!event.target.checked});
  }

  return (
    <div className="Filter">
      <label>
        <span>Check:</span>
        <input type="checkbox" checked={filter.checked} onChange={onInputChange}/>
      </label>
    </div>
  );
}
