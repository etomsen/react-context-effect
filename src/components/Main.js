import React, { useContext, useEffect } from 'react';
import './Main.css';
import { FilterContextProvider } from '../filter.context';
import { Filter } from './Filter';
import { View } from './View';

export function Main() {
  const { filter, setFilter, saving, searchId } = useContext(FilterContextProvider.context);

  /**
   * we use this callback for filter instead of using the context
   * directly from Filter, as Filter might be (is) an external component
   */
  function onChangeFilter(value) {
    if (JSON.stringify(value) !== JSON.stringify(filter)) {
      setFilter(value);
    }
  }

  // useEffect(() => {
  //   if (searchId) {
  //     window.location.search = `searchId=${searchId}`;
  //   }
  // }, [searchId])
  
  return (
    <div className="Main">
      <div className={`${saving ? 'FilterContainer FilterContainerSaving' : 'FilterContainer'}`}>
        <Filter filter={filter} onChange={onChangeFilter}></Filter>
      </div>
      <View></View>
    </div>
  );
}
