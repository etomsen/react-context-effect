import React, { useEffect, useState, useContext } from 'react';
import { FilterContextProvider } from '../filter.context';
import './View.css';

const staticData = [
  {
    title: 'Data 1',
    checked: true
  },
  {
    title: 'Data 2',
    checked: false
  },
  {
    title: 'Data 3',
    checked: true
  },
  {
    title: 'Data 4',
    checked: false
  },
  {
    title: 'Data 5',
    checked: true
  },
];

export function View() {
  const { filter } = useContext(FilterContextProvider.context);
  const [loading, setLoading ] = useState(false);
  const [data, setData] = useState([]);

  useEffect(() => {
    async function loadFilteredData() {
      setLoading(true);
      const result = await new Promise((resolve) => {
        setTimeout(() => {
          resolve(staticData.filter(i => i.checked === filter.checked));
        }, 5000);
      });
      setData(result);
      setLoading(false);
    }
    loadFilteredData();
  }, [filter, setData, setLoading]);

  const list = data.map(i => (<li>{i.title + `: ${i.checked}`}</li>));

  return (
    <div className={`${loading ? 'View ViewLoading' : 'View'}`}>
      <ul>
        {list}
      </ul>
    </div>
  );
}
