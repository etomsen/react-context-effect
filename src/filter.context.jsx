import React, { createContext, useEffect, useMemo, useState } from 'react';

const defaultFilterValue = {checked: false};
const defaultContextValue = {
  filter: defaultFilterValue,
  searchId: null,
  saving: false,
  query: null
};
const Context = createContext(defaultContextValue);

export function FilterContextProvider({children}) {
  const [searchId, setSearchId] = useState(defaultContextValue.searchId);
  const [filter, setFilter] = useState(defaultFilterValue);
  const [query, setQuery] = useState(defaultContextValue.query);
  const [saving, setSaving] = useState(defaultContextValue.saving);
  
  /**
   * effect for saving the filter on change and thus receiving the searchId
   */
  useEffect(() => {
    async function saveFilter() {
      setSaving(true);
      const result = await new Promise((resolve) => {
        setTimeout(() => {
          const filterSearchId = Math.floor(Math.random()*1000000) + '';
          resolve(filterSearchId);
        }, 2000);
      });
      setSaving(false);
      setSearchId(result);
    }
    saveFilter()
  }, [filter]);

  /**
   * legacy effect for saving the filter from query 
   */
  useEffect(() => {
    function queryToFilter(query) {
      return JSON.parse(query);
    }
    if (!!query) {
      const filter = queryToFilter(query);
      setFilter(filter)
    }
  }, [query]);

  const providerValue = useMemo(() => {
    return {
      filter,
      setFilter,
      searchId,
      setSearchId,
      saving,
      setQuery
    };
  }, [filter, searchId, saving])
  
  return (
    <Context.Provider value={providerValue}>
      {children}
    </Context.Provider>
  );
}

FilterContextProvider.context = Context;
